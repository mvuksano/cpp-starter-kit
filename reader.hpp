#include <array>
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

// Node of a tree.
template<typename T>
class Node {
public:
    T val;
    Node<T> *left;
    Node<T> *right;
    Node() {}
    Node(T val) {
        this->val = val;
    }
};

/**
 * Reader is used to convert input into various objects. 
 */
class Reader {
private:
    int insertAtPath(Node<int>* root, Node<int>* n, vector<int> path);
    vector<int> posToPath(const int pos);
public:
    template<typename T>
    int convert(ifstream& file, T& target) = delete;

    /* Convert an input file into a tree.
     *
     * \return 0 if conversion was successful.
     */
    int convert(ifstream& file, Node<int>* target);

    /* Convert an input file into a vector
     *
     * \return 0 if conversion was successful.
     */
    int convert(ifstream& file, vector<int>& target);
};

#include <iostream>
#include <gtest/gtest.h>
#include <vector>

#include "reader.hpp"

#define ARRAY_TEST_FILE "resources/test/array.txt"
#define TREE_TEST_FILE "resources/test/tree.txt"

template<typename T>
bool areSame(Node<T>* first, Node<T>* second) {
    if(first == nullptr && second == nullptr) {
        return true;
    }
    if(first == nullptr || second == nullptr) {
        return false;
    }
    if(first->val != second->val) {
        return false;
    }
    return areSame(first->left, second->left) && areSame(first->right, second->right);
}

TEST(READER, ARRAY_1) {
    Reader r;
    vector<int> results;
    std::ifstream file(ARRAY_TEST_FILE);
    r.convert(file, results);
    vector<int> expected {1, 3, 5, 7};
    ASSERT_EQ(results, expected);
}

TEST(READER, TREE_1) {
    auto buildExpectedTree = []() {
        Node<int> *root = new Node<int>(1);
        root->left = new Node<int>(2);
        root->right = new Node<int>(3);

        root->left->left = new Node<int>(4);
        root->left->right = new Node<int>(5);

        root->right->left = new Node<int>(6);
        root->right->right = new Node<int>(7);

        return root;
    };
    Reader r;
    Node<int>* root = new Node<int>();
    Node<int>* expected = buildExpectedTree();
    std::ifstream file(TREE_TEST_FILE);
    r.convert(file, root);
    ASSERT_TRUE(areSame(root, expected));
}



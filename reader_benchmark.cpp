#include <benchmark/benchmark.h>
#include <vector>
#include "reader.hpp"

#define ARRAY_TEST_FILE "resources/test/array.txt"
#define TREE_TEST_FILE "resources/test/tree.txt"

using namespace std;

static void BM_TransformVector(benchmark::State& state) {
    vector<int> results;
    Reader r;
    std::ifstream file(ARRAY_TEST_FILE);
    for (auto _ : state) {
        r.convert(file, results);
    }
}
BENCHMARK(BM_TransformVector);

static void BM_TransformVector_Large(benchmark::State& state) {
    Reader r;
    vector<int> results;
    std::ifstream file(ARRAY_TEST_FILE);
    for (auto _ : state) {
        r.convert(file, results);
    }
}
BENCHMARK(BM_TransformVector_Large);

static void BM_TransformTree(benchmark::State& state) {
    auto *root = new Node<int>();
    Reader r;
    std::ifstream file(TREE_TEST_FILE);
    for (auto _ : state) {
        r.convert(file, root);
    }
}
BENCHMARK(BM_TransformTree);

BENCHMARK_MAIN();
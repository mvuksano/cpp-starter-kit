#include <array>
#include <iostream>
#include <fstream>
#include <vector>
#include <typeinfo>
#include "reader.hpp"

using namespace std;



vector<int> Reader::posToPath(int pos) {
    vector<int> path {};
    while(pos > 1) {
        if(pos % 2 != 0) {
            path.push_back(1);
            pos -= 1;
        } else {
            path.push_back(0);
        }
        pos /= 2;
    }
    return path;
}

int Reader::insertAtPath(Node<int>* root, Node<int>* n, vector<int> path) {
    if(root == nullptr && path.size() == 0) {
        root->val = n->val;
    }
    Node<int>* current = root;
    for(int i = 0; i < path.size(); i++) {
        if(path[i] == 0) {
            if(i == path.size() - 1) {
                current->left = n;
            } else {
                current = current->left;
            }
        } else {
            if(i == path.size() - 1) {
                current->right = n;
            } else {
                current = current->right;
            }
        }
    }
    return 0;
}

int Reader::convert(ifstream& file, Node<int> *target) {
    string str;
    array<char, 16> number;
    int i = 0, curr = 0; int pos = 0;

    getline(file, str);

    if(str[0] != '[' && str[str.size() -1 ] != ']') {
        return -1;
    }

    for(int i = 1; i < str.size(); i++) {
        if(str[i] == ',' || str[i] == ']') {
            pos++;
            number[curr] = '\0';
            curr = 0;
            int val = atoi(number.data());
            if(pos == 1) {
                target->val = val;
            } else {
                vector<int> p = this->posToPath(pos);
                reverse(p.begin(), p.end());
                auto res = insertAtPath(target, new Node(val), p);
                if(res < 0) return res;
            }
        } else if (str[i] == ' ') {
            continue;
        } else {
            // TODO do more checks whether each digit is a number
            number[curr++] = str[i];
        }
    }
    return 0;
}


int Reader::convert(ifstream& file, vector<int>& target) {
    string str;
    array<char, 16> number;
    int i = 0, curr = 0;

    getline(file, str);

    if(str[0] != '[' && str[str.size() -1 ] != ']') {
        return -1;
    }

    for(int i = 1; i < str.size(); i++) {
        if(str[i] == ',' || str[i] == ']') {
            number[curr] = '\0';
            curr = 0;
            target.push_back(atoi(number.data()));
        } else if (str[i] == ' ') {
            continue;
        } else {
            // TODO do more checks whether each digit is a number
            number[curr++] = str[i];
        }
    }
    return 0;
}
